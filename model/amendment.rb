class Amendment
  attr_accessor :id, :amend_num, :amend_date, :client, :price, :content,
                :repetitions, :frequency, :transmitter
  def initialize(id, amend_num, amend_date, client, price, content,
                 repetitions, frequency, transmitter)
    @id = id
    @amend_num = amend_num
    @amend_date = amend_date
    @client = client
    @price = price
    @content = content
    @repetitions = repetitions
    @frequency = frequency
    @transmitter = transmitter
  end
end
