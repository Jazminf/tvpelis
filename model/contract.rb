require 'set'
require 'date'
require_relative 'amendment'
class Contract
  attr_accessor :id
  def initialize(id, date, client, price, content,
                 repetitions, frequency, transmitter)
    @id = id
    @creation_date = date
    @client = { date => client }
    @price = { date => price }
    @content = { date => content }
    @repetitions = { date => repetitions }
    @frequency = { date => frequency }
    @transmitter = { date => transmitter }
    @amendments = { 0 => @creation_date }
    @last_update = @creation_date
    @confirmed = false
  end

  def modify_client(client)
    raise StandardError unless is_confirmed? == false

    @client = { @creation_date => client }
  end

  def modify_price(price)
    raise StandardError unless is_confirmed? == false

    @price = { @creation_date => price }
  end

  def modify_content(content)
    raise StandardError unless is_confirmed? == false

    @content = { @creation_date => content }
  end

  def modify_repetitions(repetitions)
    raise StandardError unless is_confirmed? == false

    @repetitions = { @creation_date => repetitions }
  end

  def modify_frequency(frequency)
    raise StandardError unless is_confirmed? == false

    @frequency = { @creation_date => frequency }
  end

  def modify_transmitter(transmitter)
    raise StandardError unless is_confirmed? == false

    @transmitter = { @creation_date => transmitter }
  end

  def confirm
    @confirmed = true
  end

  def is_confirmed?
    @confirmed
  end

  def date
    @creation_date
  end

  def client
    get_last_modified_value(@client)
  end

  def price
    get_last_modified_value(@price)
  end

  def content
    get_last_modified_value(@content)
  end

  def repetitions
    get_last_modified_value(@repetitions)
  end

  def frequency
    get_last_modified_value(@frequency)
  end

  def transmitter
    get_last_modified_value(@transmitter)
  end

  def get_last_modified_value(map)
    map[map.keys.max]
  end

  def make_amend
    raise StandardError unless is_confirmed?

    amend_num = 1 + @amendments.keys.max
    @last_update = DateTime.now
    @amendments[amend_num] = @last_update
    AmendmentBuilder.new(amend_num, self)
  end

  def amend_client(amend_num,  client)
    raise StandardError unless @amendments.key?(amend_num)

    @client[@amendments[amend_num]] = client
  end

  def amend_price(amend_num, price)
    raise StandardError unless @amendments.key?(amend_num)

    @price[@amendments[amend_num]] = price
  end

  def amend_content(amend_num, content)
    raise StandardError unless @amendments.key?(amend_num)

    @content[@amendments[amend_num]] = content
  end

  def amend_repetitions(amend_num, repetitions)
    raise StandardError unless @amendments.key?(amend_num)

    @repetitions[@amendments[amend_num]] = repetitions
  end

  def amend_frequency(amend_num, frequency)
    raise StandardError unless @amendments.key?(amend_num)

    @frequency[@amendments[amend_num]] = frequency
  end

  def amend_transmitter(amend_num, transmitter)
    raise StandardError unless @amendments.key?(amend_num)

    @transmitter[@amendments[amend_num]] = transmitter
  end

  def get_amend_value(map, date)
    return map[date] if map.key?(date)

    previous_dates = map.keys.select { |day| day <= date }
    return get_last_modified_value(map) if previous_dates.empty?

    map[previous_dates.max]
  end

  def amend(amend_num)
    raise StandardError unless @amendments.key?(amend_num)

    amend_date = @amendments[amend_num]
    puts 'la fecha del amend 1 es '
    puts amend_date

    Amendment.new(@id, amend_num, amend_date,
                  get_amend_value(@client, amend_date),
                  get_amend_value(@price, amend_date),
                  get_amend_value(@content, amend_date),
                  get_amend_value(@repetitions, amend_date),
                  get_amend_value(@frequency, amend_date),
                  get_amend_value(@transmitter, amend_date))
  end

  def at(amend_date)
    amend_num = get_amend_num(amend_date)
    Amendment.new(@id, amend_num, amend_date,
                  get_amend_value(@client, amend_date),
                  get_amend_value(@price, amend_date),
                  get_amend_value(@content, amend_date),
                  get_amend_value(@repetitions, amend_date),
                  get_amend_value(@frequency, amend_date),
                  get_amend_value(@transmitter, amend_date))
  end

  def get_amend_num(date)
    puts 'se consulta con el dia'
    puts date
    puts 'amednents'
    puts @amendments
    previous_amendments = @amendments.select { |_amend_num, day| day <= date }
    puts previous_amendments
    return @amendments.keys.max unless previous_amendments.empty? == false

    previous_amendments.max[0]
  end

  def original
    Amendment.new(@id, 0, @creation_date,
                  @client[@creation_date],
                  @price[@creation_date],
                  @content[@creation_date],
                  @repetitions[@creation_date],
                  @frequency[@creation_date],
                  @transmitter[@creation_date])
  end
end

class AmendmentBuilder
  def initialize(amend_num, contract)
    @amend_num = amend_num
    @contract = contract
  end

  def with_client(client)
    @contract.amend_client(@amend_num, client)
    self
  end

  def with_price(price)
    @contract.amend_price(@amend_num, price)
    self
  end

  def with_content(content)
    @contract.amend_content(@amend_num, content)
    self
  end

  def with_repetitions(repetitions)
    @contract.amend_repetitions(@amend_num, repetitions)
    self
  end

  def with_frequency(frequency)
    @contract.amend_frequency(@amend_num, frequency)
    self
  end

  def with_transmitter(transmitter)
    @contract.amend_transmitter(@amend_num, transmitter)
    self
  end
end
