require 'rspec'
require 'set'
require 'date'
require 'date'
require_relative '../model/contract'
require_relative '../model/amendment'

describe 'Contract' do
  contract = Contract.new(id = 1, date = Date.new(2019, 0o1, 0o1), client = 'artear',
                          price = 200_000, content = ['Volver al futuro'].to_set,
                          repetitions = 1, frequency = 1, transmitter = %w[volver canal13].to_set)

  context 'given no confirmation' do
    it 'should hava an identifier' do
      expect(contract.id).to eq 1
    end

    it 'should hava a client' do
      expect(contract.client).to eq 'artear'
    end

    it 'should hava a date' do
      expect(contract.date).to eq Date.new(2019, 0o1, 0o1)
    end

    it 'should hava price' do
      expect(contract.price).to eq 200_000
    end

    it 'should hava content' do
      expect(contract.content).to eq ['Volver al futuro'].to_set
    end

    it 'should hava repetitions' do
      expect(contract.repetitions).to eq 1
    end

    it 'should hava frequency ' do
      expect(contract.frequency).to eq 1
    end

    it 'should hava a transmitter' do
      expect(contract.transmitter).to eq %w[volver canal13].to_set
    end

    it 'should not be confirmed' do
      expect(contract.is_confirmed?).to eq false
    end

    it 'can be modified if not confirmed' do
      contract.modify_price(100)
      expect(contract.price).to eq 100
    end

    it 'can not be amended if not confirmed' do
      expect do
        contract.make_amend.with_price(50_000)
      end.to raise_error(StandardError)
    end
  end

  contract2 = Contract.new(id = 1, date = Date.new(2019, 0o1, 0o1), client = 'artear',
                           price = 100, content = ['Volver al futuro'].to_set,
                           repetitions = 1, frequency = 1, transmitter = %w[volver canal13].to_set)

  context 'given confirmation: ' do
    contract2.confirm
    it 'can not be modified if is confirmed' do
      expect(contract2.is_confirmed?).to eq true
      expect do
        contract2.modify_price(20)
      end.to raise_error(StandardError)
    end

    context '1° amendment, ' do
      contract2.make_amend.with_price(50)

      it 'can be amended if is confirmed' do
        expect(contract2.is_confirmed?).to eq true
        expect(contract2.price).to eq 50
      end

      it 'can be consulted by date' do
        date = contract2.amend(1).amend_date
        amend1 = contract2.at(date)
        expect(amend1.amend_num).to eq 1
      end

      it 'return original if date is before all' do
        amend = contract2.at(Date.today - 1)
        expect(amend.amend_num).to eq 0
      end

      context '2° amendment, ' do
        contract2.make_amend.with_repetitions(7).with_client('otro-cliente')

        it 'can be amended many times if is confirmed' do
          expect(contract2.is_confirmed?).to eq true
          expect(contract2.price).to eq 50
          expect(contract2.repetitions).to eq 7
          expect(contract2.frequency).to eq 1
        end

        it 'can resolve original' do
          original = contract2.original
          expect(original.price).to eq 100
          expect(original.repetitions).to eq 1
          expect(original.frequency).to eq 1
        end

        it 'can resolve values of enmendation 1' do
          amen1 = contract2.amend(1)
          expect(amen1.price).to eq 50
          expect(amen1.repetitions).to eq 1
          expect(amen1.client).to eq 'artear'
          expect(amen1.frequency).to eq 1
        end

        it 'can resolve values of enmendation 2' do
          amen2 = contract2.amend(2)
          expect(amen2.price).to eq 50
          expect(amen2.repetitions).to eq 7
          expect(amen2.client).to eq 'otro-cliente'
          expect(amen2.frequency).to eq 1
        end

        it 'return last if date is after all' do
          amend2 = contract2.at(Date.today + 1)
          expect(amend2.amend_num).to eq 2
        end
      end
    end
  end
end
